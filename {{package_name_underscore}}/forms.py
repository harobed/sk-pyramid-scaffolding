from formencode import Schema, validators


class UserSchema(Schema):
    allow_extra_fields = True
    filter_extra_fields = True

    firstname = validators.UnicodeString()
    lastname = validators.UnicodeString()
    mail = validators.Email()
    staff = validators.Bool()
    api_key = validators.UnicodeString()
