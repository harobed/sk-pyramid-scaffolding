$(document).ready(function() {
    $('BODY.list TABLE.entity-list').on('click', '.delete-action', function() {
        var $tr = $(this).parents('TR').one();
        bootbox.confirm("Are you sure to delete " + $tr.data('display') + " ?", function(result) {
            if (result) {
                document.location.href = pyramid.route_url('backoffice_delete_user', {'user_id': $tr.data('id')}) + '?come_from=' + pyramid.url;
            }
        });
    });
});
