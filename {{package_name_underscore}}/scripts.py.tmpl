# -*- coding: utf8 -*-
"""{{package_name}} cli tool

Usage:
    {{package_name}} init <config_uri> [--reset]
    {{package_name}} fixtures <config_uri>
    {{package_name}} create-admin-user <config_uri> [--mail=<mail> --password=<password>]

Options:
    --reset         Drop database before initialise
    -v, --verbose

Example:

$ {{package_name}} init development.ini

"""
try:
    # this raw_input is not converted by 2to3
    term_input = raw_input
except NameError:
    term_input = input

from pyramid.paster import get_appsettings, setup_logging
from docopt import docopt

from {{package_name_underscore}} import models
from {{package_name_underscore}} import fixtures
from {{package_name_underscore}}.utils import generate_api_key


def main():
    arguments = docopt(__doc__)

    if arguments['<config_uri>'] is not None:
        setup_logging(arguments['<config_uri>'])
        settings = get_appsettings(arguments['<config_uri>'])

    if arguments['fixtures'] or arguments['--reset']:
        models.initialize_db(settings, drop_all=True)
    else:
        models.initialize_db(settings)

    if arguments['fixtures']:
        fixtures.populate_users()

    elif arguments['create-admin-user']:
        if (
            arguments['--mail'] and
            arguments['--password']
        ):
            create_admin_user(
                arguments['--mail'],
                arguments['--password']
            )
        else:
            _create_admin_user_prompt()


def _create_admin_user_prompt():
    print('Create new admin user')

    mail = term_input('mail : ')
    password = term_input('password : ')

    create_admin_user(mail, password)


def create_admin_user(mail, password):
    u = models.User(
        mail=unicode(mail),
        password=password,
        staff=True,
        api_key=generate_api_key()
    )
    models.DBSession.add(u)
    models.DBSession.commit()
