__version__ = '0.1.0'

import os
import json
#import locale

from sqlalchemy import engine_from_config
from pyramid.events import BeforeRender, NewRequest
from pyramid.config import Configurator
from pyramid.scripts.pserve import watch_file
from pyramid.i18n import get_localizer, TranslationStringFactory
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.httpexceptions import HTTPFound
from pyramid.security import Allow
import pyramid_webassets
from furl import furl
from pyramid_multiauth import MultiAuthenticationPolicy

import {{package_name_underscore}}.utils
from {{package_name_underscore}}.models import DBSession, Base, User
from {{package_name_underscore}}.restful import init as init_rest
from {{package_name_underscore}}.restful import RestAPIKeyAuthenticationPolicy
from {{package_name_underscore}}.utils import which, WebassetsYAMLLoader, JSONEncoder

here = os.path.abspath(os.path.dirname(__file__))

#locale.setlocale(locale.LC_ALL, 'fr_FR')


def add_renderer_globals(event):
    request = event['request']

    event['authenticated_user'] = User.get_from_request(request)
    event['_'] = request.translate
    event['localizer'] = request.localizer
    event['url'] = request.route_url
    event['current_url'] = furl(request.url)

tsf = TranslationStringFactory('{{package_name_underscore}}')


def add_localizer(event):
    request = event.request
    localizer = get_localizer(request)

    def auto_translate(*args, **kwargs):
        return localizer.translate(tsf(*args, **kwargs))

    request.localizer = localizer
    request.translate = auto_translate


def json_renderer_factory(info):
    def _render(value, system):
        request = system.get('request')
        if request is not None:
            response = request.response
            ct = response.content_type
            if ct == response.default_content_type:
                response.content_type = 'application/json'

        if isinstance(value, {{package_name_underscore}}.utils.JsonWithContext):
            context = value.context
            value = value.value
        else:
            context = {}

        context['request'] = request

        return json.dumps(
            value,
            cls=JSONEncoder,
            indent=4 if system['request'].registry.settings['pretty_json'] else None,
            settings=context
        )

    return _render


def check_dependencies():
    error = False
    for exe, msg in (
        ('sass', '    $ gem install sass'),
        ('compass', '    $ gem install compass')
    ):
        if not which(exe):
            error = True
            print('Error: %s executable missing, install it with : \n\n%s\n' % (exe, msg))

    if not os.path.exists(os.path.join(here, 'static', 'components')):
        error = True
        print('Error : assets missing \n\n    $ bower install\n')

    if error:
        import sys
        sys.exit()


class RootFactory(object):
    __acl__ = [
        (Allow, 'group:manager', 'manage')
    ]

    def __init__(self, request):
        pass


def groupfinder(mail, request):
    u = User.get_by_mail(mail)
    if u and u.staff:
        return ['group:manager']

    return ['guest']


def groupfinder_by_api_key(api_key, request):
    u = User.get_by_api_key(api_key)
    if u and u.staff:
        return ['group:manager']

    return ['guest']


def forbidden_view(request):
    return HTTPFound(
        location=request.route_url('signin')
    )


def main(global_config, **settings):
    check_dependencies()
    settings['pretty_json'] = settings.get('pretty_json', 'False').lower() == 'true'
    if not settings.get('test_mode', False):
        engine = engine_from_config(settings, 'sqlalchemy.')
        DBSession.configure(bind=engine)
        Base.metadata.bind = engine

    if 'date.today' in settings:
        {{package_name_underscore}}.utils.fixed_date = settings['date.today'].split('-')

    config = Configurator(settings=settings, root_factory=RootFactory)
    app_config(config)

    return config.make_wsgi_app()


def app_config(config):
    pyramid_webassets.includeme(config)
    config.include('pyramid_beaker')
    config.include('pyramid_js')
    config.add_translation_dirs('{{package_name_underscore}}:locale')

    policies = [
        AuthTktAuthenticationPolicy('seekrit', hashalg='sha512', callback=groupfinder),
        RestAPIKeyAuthenticationPolicy(callback=groupfinder_by_api_key)
    ]
    authz_policy = ACLAuthorizationPolicy()
    config.set_authentication_policy(MultiAuthenticationPolicy(policies))

    config.set_authorization_policy(authz_policy)
    config.add_forbidden_view(forbidden_view)

    config.add_subscriber(add_renderer_globals, BeforeRender)
    config.add_subscriber(add_localizer, NewRequest)
    config.add_renderer('json', json_renderer_factory)
    init_rest(config)

    config.add_route('root_url', '/')
    config.add_view('{{package_name_underscore}}.views.root_url', route_name='root_url')

    config.add_route('backoffice_dashboard', '/admin/')
    config.add_view('{{package_name_underscore}}.views.backoffice_dashboard', route_name='backoffice_dashboard', permission='manage')

    config.add_route('backoffice_users', '/admin/users/')
    config.add_view('{{package_name_underscore}}.views.backoffice_users', route_name='backoffice_users', permission='manage')

    config.add_route('backoffice_edit_user', '/admin/users/{user_id}/')
    config.add_view('{{package_name_underscore}}.views.backoffice_edit_user', route_name='backoffice_edit_user', permission='manage')

    config.add_route('backoffice_delete_user', '/admin/users/{user_id}/delete')
    config.add_view('{{package_name_underscore}}.views.backoffice_delete_user', route_name='backoffice_delete_user', permission='manage')

    config.add_route('signin', '/signin.html')
    config.add_view('{{package_name_underscore}}.views.signin', route_name='signin')

    config.add_route('signup', '/signup.html')
    config.add_view('{{package_name_underscore}}.views.signup', route_name='signup')

    config.add_route('signout', '/signout.html')
    config.add_view('{{package_name_underscore}}.views.signout', route_name='signout')

    loader = WebassetsYAMLLoader(os.path.join(here, 'webassets.yaml'))
    watch_file(os.path.join(here, 'webassets.yaml'))
    for k, v in loader.load_bundles().items():
        config.add_webasset(k, v)
